/*
 src/reducers/rootReducer.js
*/
import { combineReducers } from 'redux';
import simpleReducer from './simpleReducer';
import symbolSearch from '../components/symbolSearch/reducer';
import chartComponent from '../components/chartComponent/reducer';

export default combineReducers({
 simpleReducer,
 symbolSearch,
 chartComponent
});