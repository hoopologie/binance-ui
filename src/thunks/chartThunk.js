// fetchUserDetails thunk
// It returns a function that takes dispatch as the first argument. When AJAX 
// request is successful, it dispatches getUserSuccess action with user object.
const fetchChartData = (username) => {
    return dispatch => {
     return gitHubApi(username)
       .then(user => {
          dispatch(getUserSuccess(user))
      })
       .catch(error => { throw error; })
    }
  }