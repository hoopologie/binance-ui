import React, { Component } from 'react';
import { connect } from 'react-redux';

import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'
import { getChartData } from './reducer';

class ChartComponent extends Component {

  getOptions = () => {
    return {
        title: {
          text: 'My stock chart'
        },
        series: [{
          data: [1, 2, 3]
        }]
      };
   }

  componentDidMount() {
    this.props.getChartData({symbol:'BTCUSDT', interval:'5m'});
  }

  render() {

    debugger;
   return (
    <HighchartsReact
        highcharts={Highcharts}
        constructorType={'stockChart'}
        options={this.getOptions()}
        />
   );
  }
 }

 const mapStateToProps = state => {
   var data = state.chartComponent.data;
    
   return {
     rawData : data
   };
 };

 const mapDispatchToProps = {
  getChartData
};



export default connect(mapStateToProps, mapDispatchToProps)(ChartComponent);

