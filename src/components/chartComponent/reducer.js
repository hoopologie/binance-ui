import axios from 'axios';
import { apiBaseUrl } from '../../constants';

export const GETCHARTDATA = 'CHARTDATALOAD';
export const GETCHARTDATA_SUCCESS = 'CHARTDATALOAD_SUCCESS';
export const GETCHARTDATA_FAIL = 'CHARTDATALOAD_FAIL';

export default function reducer(state = { symbols: [] }, action) { // setting the initial state
  switch (action.type) {
    case GETCHARTDATA:
      return { ...state, loading: true };
    case GETCHARTDATA_SUCCESS:
      return { ...state, loading: false, data: action.payload };
    case GETCHARTDATA_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };
    default:
      return state;
  }
}

//'/candles/:symbol/:interval/',
export function getChartData(args) {
    return function(dispatch) {
        dispatch({type: GETCHARTDATA});
        axios.get(apiBaseUrl + 'binance/candles/' + args.symbol + '/' + args.interval)
        .then(response => {
            dispatch({
                type: GETCHARTDATA_SUCCESS,
                payload: response.data
            });
        })
        .catch((error) => {
            dispatch({type: GETCHARTDATA_FAIL});
        })
    }
}