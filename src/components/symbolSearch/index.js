import React from 'react';
import Autosuggest from 'react-autosuggest';
import { connect } from 'react-redux';
import {symbolList} from './reducer'


/* --------------- */
/*    Component    */
/* --------------- */

class App extends React.Component {
    constructor() {
        super();

        this.state = {
            value: '',
            suggestions: [],
            isLoading: true
        };
    }

    componentDidMount() {
       this.props.symbolList();
    }

    getSuggestions = value => {
        var matches = this.props.symbolCache.filter(item => {
            return item.indexOf(value) >= 0;
        });
        return matches;
    };

    getSuggestionValue(suggestion) {
        return suggestion;
    }

    renderSuggestion(suggestion) {
        return (
            <span>{suggestion}</span>
        );
    }

    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        });
    };

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value)
          });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const { value, suggestions } = this.state;
        const inputProps = {
            placeholder: "Type 'BTC'",
            value,
            onChange: this.onChange
        };

        return (
            <div>
                <Autosuggest
                    suggestions={suggestions}
                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                    getSuggestionValue={this.getSuggestionValue}
                    renderSuggestion={this.renderSuggestion}
                    getSuggestions={this.getSuggestions}
                    inputProps={inputProps} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    let symbols = state.symbolSearch.symbols;
    return {
        symbolCache: symbols,
        value: '',
        suggestions: []
    };
  };
  
  const mapDispatchToProps = {
    symbolList
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(App);