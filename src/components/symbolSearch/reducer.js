import axios from 'axios';
import { apiBaseUrl } from '../../constants';

export const GET_SYMBOLS = 'SYMBOLS_LOAD';
export const GET_SYMBOLS_SUCCESS = 'SYMBOLS_LOAD_SUCCESS';
export const GET_SYMBOLS_FAIL = 'SYMBOLS_LOAD_FAIL';

export default function reducer(state = { symbols: [] }, action) { // setting the initial state
  switch (action.type) {
    case GET_SYMBOLS:
      return { ...state, loading: true };
    case GET_SYMBOLS_SUCCESS:
      return { ...state, loading: false, symbols: action.payload };
    case GET_SYMBOLS_FAIL:
      return {
        ...state,
        loading: false,
        error: 'Error while fetching repositories'
      };
    default:
      return state;
  }
}

export function symbolList() {
    return function(dispatch) {
        dispatch({type: GET_SYMBOLS});
        axios.get(apiBaseUrl + 'binance/symbollookup/')
        .then(response => {
            dispatch({
                type: GET_SYMBOLS_SUCCESS,
                payload: response.data
            });
        })
        .catch((error) => {
            dispatch({type: GET_SYMBOLS_FAIL});
        })
    }
}