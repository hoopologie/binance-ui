import React, { Component } from 'react';
import { connect } from 'react-redux';
import { simpleAction } from './actions/simpleAction';
import ChartComponent from './components/chartComponent';
import SymbolSearch from './components/symbolSearch';

import logo from './logo.svg';
import './App.css';

class App extends Component {

  simpleAction = (event) => {
    this.props.simpleAction();
   }

  render() {
   return (
    <div className="App">
     <header className="App-header">
      <h1 className="App-title">Napoleonic Research</h1>
      <SymbolSearch />
      <div class="main">
        <ChartComponent />
      </div>
     </header>
    </div>
   );
  }
 }

 const mapStateToProps = state => ({
  ...state
 });

 const mapDispatchToProps = dispatch => ({
  simpleAction: () => dispatch(simpleAction())
 });



export default connect(mapStateToProps, mapDispatchToProps)(App);
